CFLAGS = -std=c11 -g
LDFLAGS = -lm

main: algorithms.o process.o queue.o server.o scheduler.o

scheduler.o: process.o queue.o server.o

clean:
	rm -f *.o main
