#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "algorithms.h"
#include "process.h"
#include "server.h"
#include "queue.h"
#include "scheduler.h"

#define check(pointer) \
  if (!pointer) { \
    printf(#pointer " can't be null!\n"); \
    goto clean; \
  }

void print_snapshot(scheduler *sched, server *serv, queue *q, process *p, size_t time);
size_t get_next_arrival_time(size_t time);
size_t get_next_processing_time(void);

int main(int argc, char *argv[]) {

  process *next_process = NULL;
  queue *server_queue = NULL;
  server *serv = NULL;
  scheduler *sched = NULL;

  if (argc != 2) {
    printf("Usage: %s tempo_de_simulação\n", argv[0]);
    return 1;
  }

  size_t simulation_time = strtoul(argv[1], NULL, 0);

  // start randomization
  srand(time(NULL));

  // Get arrival_time and processing_time of the first process
  size_t next_arrival_time = get_next_arrival_time(0);
  size_t next_processing_time = get_next_processing_time();

  // Create first process
  next_process = process_new(next_arrival_time, next_processing_time);
  check(next_process);

  // Create queue
  server_queue = queue_new();
  check(server_queue);

  // Create server
  serv = server_new();
  check(serv);

  // create scheduler
  sched = scheduler_new();
  check(sched);
  scheduler_set_simulation_time(sched, simulation_time);

  // start simulation loop
  for (size_t time = 1; time <= simulation_time; ++time) {
    // Check if there is a process will arrive at the current time
    if (process_arrival_time(next_process) == time) {
      // Add process to the queue
      queue_process_add(server_queue, next_process);
      // Create next process
      next_arrival_time = get_next_arrival_time(time);
      next_processing_time = get_next_processing_time();
      next_process = process_new(next_arrival_time, next_processing_time);
      check(next_process);
    }
    // Check if the server is busy
    if (server_is_busy(serv)) {
      // Check if the process on the server finished
      if (scheduler_process_finished(sched, time)) {
        scheduler_finish_process(sched, serv, time);
        scheduler_process_queue(sched, server_queue, serv, time);
      }
    } else {
      scheduler_process_queue(sched, server_queue, serv, time);
    }
    scheduler_update_queue_size_metric(sched, server_queue);
    print_snapshot(sched, serv, server_queue, next_process, time);
  }
  scheduler_print_performance_metrics(sched);

  // Free allocated structures
clean:
  free(next_process);
  queue_delete(&server_queue);
  server_delete(&serv);
  scheduler_delete(&sched);
  return 0;
}

void print_snapshot(scheduler *sched, server *serv, queue *q, process *p, size_t time) {
  printf("Time       Process on the queue     Server is busy?  Next process arrival time\n");
  printf("%5d      %5d                    %d                %d\n", time, queue_size(q), server_is_busy(serv), process_arrival_time(p));
}

size_t get_next_arrival_time(size_t time) {
  return time + uniform_distribution(1, 8);
}

size_t get_next_processing_time(void) {
  return triangular_distribution(2, 3, 7);
}
