Process scheduling simulator done as an assignment of the course PCS 2039 - 
Modeling and Simulation of Computer Systems.

###Usage
Compile it typing `make`

Run simulation for `n` seconds doing:

    $ ./main n
