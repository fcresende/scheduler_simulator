#include "queue.h"

struct node {
  process *p;
  node *next;
};

queue *queue_new(void) {
  queue *q = malloc(sizeof(queue));
  *q = (queue){ .first = NULL,
         .last = NULL,
         .size = 0
       };
  return q;
}

void queue_process_add(queue *self, process *p) {
  node *n = malloc(sizeof(node));
  *n = (node){ .p = p, .next = NULL };
  if (self->size == 0) {
    self->first = n;
    self->last = n;
  } else {
    self->last->next = n;
    self->last = n;
  }
  ++(self->size);
}

process *queue_process_remove(queue *self) {
  if (self->size == 0) {
    return NULL;
  }
  node *n = self->first;
  self->first = n->next;
  --(self->size);
  process *p = n->p;
  free(n);
  return p;
}

size_t queue_size(queue *self) {
  return self->size;
}

void queue_delete(queue **self) {
  if (*self == NULL) return;
  while (queue_size(*self)) {
    process *p = queue_process_remove(*self);
    free(p);
  }
  free(*self);
  *self = NULL;
}
