#ifndef __SCHEDULER_H__
#define __SCHEDULER_H__
#include <stdlib.h>
#include <stdbool.h>

typedef struct queue queue;
typedef struct server server;

typedef struct scheduler {
  size_t sum_process_queue_time;
  size_t num_process_served;
  size_t sum_process_system_time;
  size_t server_busy_time;
  size_t simulation_time;
  size_t *num_process_on_queue;
  size_t num_process_on_queue_size;
  size_t current_process_finish_time;
} scheduler;

// Create a new scheduler
scheduler *scheduler_new(void);

// Move process from queue to server
void scheduler_process_queue(scheduler *self, queue *q, server *s, size_t time);

// Remove finished process from server
void scheduler_finish_process(scheduler *self, server *s, size_t time);

// Update queue size metric information
void scheduler_update_queue_size_metric(scheduler *self, queue *q);

// Set simulation time
void scheduler_set_simulation_time(scheduler *self, size_t time);

// Check if the server finished processing the process
bool scheduler_process_finished(scheduler *self, size_t time);

// Free scheduler
void scheduler_delete(scheduler **self);

// Print performance metrics information
void scheduler_print_performance_metrics(scheduler *self);
#endif
