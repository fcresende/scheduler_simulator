#ifndef __SERVER_H__
#define __SERVER_H__
#include <stdbool.h>

typedef struct process process;

typedef struct server {
  process *current_process;
} server;

// Create new server
server *server_new(void);

// Add process to server
void server_process_add(server *self, process *p);

// Remove process from server
process *server_process_remove(server *self);

// Check if server is busy
bool server_is_busy(server *self);

// Free server
void server_delete(server **self);
#endif
