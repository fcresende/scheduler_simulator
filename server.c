#include "server.h"
#include <stdlib.h>

server *server_new(void) {
  server *s = malloc(sizeof(server));
  s->current_process = NULL;
  return s;
}

void server_process_add(server *self, process *p) {
  self->current_process = p;
}

process *server_process_remove(server *self) {
  process *p = self->current_process;
  self->current_process = NULL;
  return p;
}

bool server_is_busy(server *self) {
  return self->current_process != NULL;
}

void server_delete(server **self) {
  if (*self == NULL) return;
  process *p = server_process_remove(*self);
  free(p);
  free(*self);
  *self = NULL;
}
