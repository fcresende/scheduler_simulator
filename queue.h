#ifndef __QUEUE_H__
#define __QUEUE_H__
#include <stdlib.h>

typedef struct node node;
typedef struct process process;

typedef struct queue {
  node *first, *last;
  size_t size;
} queue;

// Create a new queue
queue *queue_new(void);

// Add process to queue
void queue_process_add(queue *self, process *p);

// Remove process from queue
process *queue_process_remove(queue *self);

// Get queue size
size_t queue_size(queue * self);

// Free queue and its elements
void queue_delete(queue **self);
#endif
