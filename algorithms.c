#include "algorithms.h"
#include <math.h>

size_t uniform_distribution(size_t a, size_t b) {
  float y = 1.0*rand()/RAND_MAX;
  return (a + y*(b - a)) + 0.5;
}

size_t triangular_distribution(size_t a, size_t m,  size_t b) {
  float y = 1.0*rand()/RAND_MAX; // y between 0 and 1
  if (y <= (m - a)/(b - a)) {
    return (a + sqrt(y * (m - a) * (b - a))) + 0.5;
  } else {
    return (b - sqrt( (b - m) * (b - a) * (1 - y) )) + 0.5;
  }
}
