#ifndef __PROCESS_H__
#define __PROCESS_H__
#include <stdlib.h>

typedef struct process {
  size_t arrival_time;
  size_t processing_time;
} process;

// Create a new process
process *process_new(size_t arrival_time, size_t processing_time);

// Return the arrival time of the process
size_t process_arrival_time(process *self);

// Return the processing time of the process
size_t process_processing_time(process *self);
#endif
