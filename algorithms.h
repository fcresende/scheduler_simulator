#ifndef __ALGORITHMS_H__
#define __ALGORITHMS_H__
#include <stdlib.h>

size_t uniform_distribution(size_t a, size_t b);

size_t triangular_distribution(size_t b, size_t moda,  size_t a);

#endif
