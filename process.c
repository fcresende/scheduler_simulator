#include "process.h"

process *process_new(size_t arrival_time, size_t processing_time) {
  process *p = malloc(sizeof(process));
  p->arrival_time = arrival_time;
  p->processing_time = processing_time;
  return p;
}

size_t process_arrival_time(process *self) {
  return self->arrival_time;
}

size_t process_processing_time(process *self) {
  return self->processing_time;
}
