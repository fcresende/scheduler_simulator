#include "scheduler.h"
#include "process.h"
#include "server.h"
#include "queue.h"

#include <stdio.h>

scheduler *scheduler_new(void) {
  scheduler *s = malloc(sizeof(scheduler));
  size_t *array = calloc(10, sizeof(size_t));
  *s = (scheduler){
         .sum_process_queue_time = 0,
         .num_process_served = 0,
         .sum_process_system_time = 0,
         .server_busy_time = 0,
         .simulation_time = 0,
         .num_process_on_queue = array,
         .num_process_on_queue_size = 10,
         .current_process_finish_time = 0
       };
  return s;
}

void scheduler_process_queue(scheduler *self, queue *q, server *s, size_t time) {
  // Get next process to be served
  process *p = queue_process_remove(q);
  if (p) {
    // Update total time spent by processes on the queue
    self->sum_process_queue_time += (time - process_arrival_time(p));
    // Update time the server will have finished processing the process
    self->current_process_finish_time = time + process_processing_time(p);

    if (self->current_process_finish_time > self->simulation_time) {
      self->server_busy_time += process_processing_time(p) - (self->current_process_finish_time - (self->simulation_time + 1));
    }
    // Assign process to server
    server_process_add(s, p);
  }
}

void scheduler_finish_process(scheduler *self, server *s, size_t time) {
  // Revemo process from server
  process *p = server_process_remove(s);
  // Update total time spent by the process on the system
  self->sum_process_system_time += (time - process_arrival_time(p));
  // Update number of served processes
  ++(self->num_process_served);
  // Update server busy time
  self->server_busy_time += process_processing_time(p);
  free(p);
}

void scheduler_update_queue_size_metric(scheduler *self, queue *q) {
  // Get number of processes on the queue
  size_t n = queue_size(q);
  // Allocate more space, if necessary
  if (n >= self->num_process_on_queue_size) {
    self->num_process_on_queue = realloc(self->num_process_on_queue, 2*n * sizeof(size_t));
    for (size_t i = self->num_process_on_queue_size; i < 2*n; ++i) {
      self->num_process_on_queue[i] = 0;
    }
    self->num_process_on_queue_size = 2*n;
  }
  // Update information about how much time the queue has n processes
  ++(self->num_process_on_queue[n]);
}

void scheduler_set_simulation_time(scheduler *self, size_t time) {
  self->simulation_time = time;
}

void scheduler_print_performance_metrics(scheduler *self) {
  float queue_process_mean_time = 1.0*self->sum_process_queue_time/self->num_process_served;
  float system_process_mean_time = 1.0*self->sum_process_system_time/self->num_process_served;
  float server_utilization = 1.0*self->server_busy_time/self->simulation_time;
  size_t sum = 0;
  for (size_t i = 0; i < self->num_process_on_queue_size; ++i) {
    sum += i * self->num_process_on_queue[i];
  }
  float queue_process_mean_number = 1.0*sum/self->simulation_time;
  printf("Duração da simulação: %d\n", self->simulation_time);
  printf("Tempo médio de processos (atendidos) na fila: %f\n", queue_process_mean_time);
  printf("Tempo médio de processos (atendidos) no sistema: %f\n", system_process_mean_time);
  printf("Número médio de processos na fila: %f\n", queue_process_mean_number);
  printf("Utilização do servidor: %f\n", server_utilization);
}

bool scheduler_process_finished(scheduler *self, size_t time) {
  return time >= self->current_process_finish_time;
}

void scheduler_delete(scheduler **self) {
  if (*self == NULL) return;
  free((*self)->num_process_on_queue);
  free(*self);
  *self = NULL;
}
